# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Forum]
* Key functions (add/delete)
    1. [post list page]
    2. [post page]
    3. [leave comment under any post]
    4. [user page]
* Other functions (add/delete)
    1. [可以上傳頭貼，放在個人頁面中]
    2. [可以查看論壇中po文者的個人頁面，只要按po文下的Owner's page按鈕即可跳轉]
    3. [未曾上傳頭像者，會自動顯示一張預設頭像。用google帳號登入時，預設頭像會是你的google頭像。當然，前提是你的google帳號沒有隱私權設定擋住。]
    4. [每篇po文左側會顯示po文者的最新頭貼]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

1.能夠創建帳號並登入，或者使用google帳號登入。
2.創建帳號時不符格式，或是以不存在的帳號試圖登入都會顯示錯誤訊息。
3.登入後能夠在個人頁面填寫資料，如果是初始帳號只會秀出帳號email，不過到Make my page中完成資料填寫後，便能在個人頁面展示出來。
4.除填寫資料外，也能上傳圖片作為頭貼，初始帳號則會有一張可愛的圖做為預設頭像
5.若google帳號本身有頭貼，且隱私權設定並未擋住firebase則會直接拿它作為預設頭貼。(如果firebase拿不到google頭貼，則會顯示upload your picture建議使用者在個人頁面上傳頭像)
6.於論壇首頁中可以發文，會實時顯示新增的貼文以外，有更新時也能讓所有論壇中的使用者收到google通知。
7.若想要查看某個貼文的回覆串或者進行回覆，能夠點擊該貼文的Post Page按鈕。進入post page後即能針對該文進行回復。
8.在首頁中若想拜訪po文主的頁面，能夠點擊該貼文的Owner's Page按鈕，便能夠看見對方的個人頁面。
9.看別人的頁面時，底下的Make my page按鈕仍是更改"自己"的頁面，無法動別人的頁面哦!
10.所有貼文和回覆左側都會顯示該使用者的最新頭貼。
11.登出時會有alert顯示登出訊息，並自動跳轉至登入頁面。(若在未登入的情況下直接以網址進入個人頁面或者make my page頁面則會跳出alert顯示訊息，並跳轉至登入頁面)


註:以上所有使用者資料皆以database或者storage儲存。
註:bootstrap template->Offcanvas, Side Navigation, Sign-in/ API->jquery .animate()

*小功能講解:
1.點擊左上角首頁名稱:Arcadia便能跳轉至首頁。
2.個人頁面和make my page中點擊左上角圖示能拉出側邊的隱藏選單，再點一次同樣的圖示或者選單中的叉叉符號便能收回選單。(頁面會配合選單滑動)

*firebase:https://software-studio-105062301.firebaseapp.com/

## Security Report (Optional)
於論壇首頁中必須登入才能看見或者更新貼文，其他頁面若偵測到未登入則會自動跳轉登入頁面。因為能防止未登入者直接輸入網址，基本上可以不用擔心論壇被隨意更改。
另外，如上面所述就算登入了，觀看其他使用者頁面時也無法更改對方的資料，所以不用擔心資料被其他人更改。


