var flag = 0;

function openNav() {
    if(flag==0){         
        document.getElementById("mySidenav").style.width = "200px";
        document.getElementById("bd").style.marginLeft = "200px";      
        flag = 1;
    }
    else{
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("bd").style.marginLeft = "0%";
        flag = 0;
    }
    
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("bd").style.marginLeft = "0%";
    flag = 0;
}

function logout(){
    firebase.auth().signOut();
    window.location = "signin.html";
}


function make(){
    window.location = "makemydata.html";
}


function init() {
    var user_email = '';
    var user_imgurl = '';
   firebase.auth().onAuthStateChanged(function (user) {
        
        if (user) {
            user_email = user.email;         
                var dataRef = firebase.database().ref(user.uid);
                    dataRef.once('value')
                    .then(function (snapshot) {
                        var Data = snapshot.val();
                        var name = Data.username;
                        var sex = Data.sex;
                        var birth = Data.birth;
                        var hobby = Data.hobby;
                        var motto = Data.motto;
                        var menu = document.getElementById('data_list');
                        menu.innerHTML = "<a>Name: "+ name +"</a><br><a>Sex: "+ sex +"</a><br><a>Birth: "+ birth +"</a><br><a>Hobby: "+ hobby +"</a><br><a>Favorite Motto: "+ motto +"</a>";
                           
                    
                    })
                    .catch(e => console.log(e.message));

                    var menu = document.getElementById('Img');
                    if(user.photoURL == null){
                        user_imgurl ='img/imouto.jpg';
                    }
                    else{
                        user_imgurl = user.photoURL;
                    }
                    menu.innerHTML = "<a style='margin:0px; padding:0px'><img id='top-right-img' src='" + user_imgurl + "' alt='Upload Your Picture!' style='width:80%; height:80%; margin:0px; padding:0px'></a><br><a id='top-right-email'>Account: "
                     + user.email + "</a>";
               
        } 
        else{
            alert("Please Log in");
            window.location = "signin.html";
        }
    
   });
}

    window.onload = function () {
        init();
    }
    
