function postpage(key){
    
    window.location = "postpage.html?"+key;
}
function postowner(id){
    window.location = "otherdata.html?"+id;
}


function init() {
    var user_email = '';
    var user_img = 'img/imouto.jpg';
    var user_id = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            user_id = user.uid;
            menu.innerHTML = "<span>"+user.email+"</span><span class='dropdown-item' id='mypage-btn'>Mypage</span>"+"<span class='dropdown-item' id='logout-btn'>Logout</span>";
              
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out')
                        window.location = "signin.html";
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            var mypage_button = document.getElementById('mypage-btn');
            mypage_button.addEventListener('click', function () {
                window.location = "mydata.html";
            });

            var str_after_content = "</p></div></div>\n";


            var postsRef = firebase.database().ref('com_list');
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
                postsRef.once('value')
                    .then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {
                            var childData = childSnapshot.val();
                            var storageRef = firebase.storage().ref(childData.userid + "/Img.jpg");
                            var str_before_username = "<div class='my-3 p-3 rounded box-shadow' style= 'background-color:rgba(255, 255, 255, 0.6)'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='' alt='' class='"+childData.userid+"' style='height:50px; width:50px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                            storageRef.getDownloadURL().then(function(url) {
                                user_img = url;
                                $("."+childData.userid).each(function(){
                                    this.src = user_img;
                                });
                            }).catch(function(error){
                                if(user.photoURL == null){
                                    user_img ='img/imouto.jpg';
                                }
                                else{
                                    user_img = user.photoURL;
                                }
                               $("."+childData.userid).each(function(){
                                   this.src = user_img;
                               });
                            });
                            //console.info(user_img)
  
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + "<br><button type='button' onclick='postpage(&apos;"+childSnapshot.key+"&apos;)'>Post page</button><button type='button' onclick='postowner(&apos;"+childData.userid+"&apos;)'>Owner's page</button>"+ str_after_content;
                            first_count += 1;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                        });
                
                //add listener
                postsRef.on('child_added', function (data) {
                    
    
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData = data.val();
                        var storageRef = firebase.storage().ref(childData.userid + "/Img.jpg");
                        storageRef.getDownloadURL().then(function(url) {
                            user_img = url;
                            $("."+childData.userid).each(function(){
                                this.src = user_img;
                            });
                        }).catch(function(error){
                            if(user.photoURL == null){
                                user_img ='img/imouto.jpg';
                            }
                            else{
                                user_img = user.photoURL;
                            }
                            $("."+childData.userid).each(function(){
                                this.src = user_img;
                            });
                        });
                        //console.info(user_img)
                        var str_before_username = "<div class='my-3 p-3 rounded box-shadow' style= 'background-color:rgba(255, 255, 255, 0.6)'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='' alt='' class='"+childData.userid+"' style='height:50px; width:50px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                        if (Notification.permission !== "granted")
                            Notification.requestPermission();
                        else {
                            var notification = new Notification('Notification', {
                                icon: 'img/icon.png',
                                body: "A message have been send to Arcadia",
                            });
                            notification.onclick = function () {
                                window.open("https://software-studio-105062301.firebaseapp.com");      
                            };
                            init();
                        }
                          
                        
                    }
                    
                    
                });
            })
            .catch(e => console.log(e.message));

            
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    document.addEventListener('DOMContentLoaded', function () {
        if (!Notification) {
            alert('Desktop notifications not available in your browser. Try Chromium.'); 
            return;
        } 
        if (Notification.permission !== "granted")
            Notification.requestPermission();
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');


    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value,
                userid: user_id 
                //url: 
            });
            post_txt.value = "";
            
        }
    });   
        
}



window.onload = function () {
    init();
}