var flag = 0;

function openNav() {
    if(flag==0){
        document.getElementById("mySidenav").style.width = "200px";
        document.getElementById("bd").style.marginLeft = "200px";
        flag = 1;
    }
    else{
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("bd").style.marginLeft = "0%";
        flag = 0;
    }
    
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("bd").style.marginLeft = "0%";
    flag = 0;
}

function logout(){
    firebase.auth().signOut();
    window.location = "signin.html";
}
    
    
    
    
    //var sure = document.getElementById('sure');
    
    function submit(){
        var name_txt = document.getElementById('name');
        var sex_txt = document.getElementById('sex');
        var birth_txt = document.getElementById('birth');
        var hobby_txt = document.getElementById('hobby');
        var motto_txt = document.getElementById('motto');
        var user = firebase.auth().currentUser;
        var dataref = firebase.database().ref(user.uid);
        dataref.set({
            email: user.email,
            username: name_txt.value,
            sex: sex_txt.value,
            birth: birth_txt.value,
            hobby: hobby_txt.value,
            motto: motto_txt.value

        });
        
        alert("Success");
        //window.location = "mydata.html";
    }

    // Upload

    
    var selectedFile;
    
    var up = document.addEventListener('change',e => {
        var user1 = firebase.auth().currentUser;
        var storageRef = firebase.storage().ref(user1.uid + "/Img.jpg");
        var reader = new FileReader();
        reader.readAsDataURL(selectedFile);
        reader.addEventListener('load', function(e){
            var URL = this.result;
            storageRef.putString(this.result,'data_url').then(function(snapshot) {
                console.log('Uploaded a raw string!');
                storageRef.getDownloadURL().then(function(url) {
                    user1.updateProfile({
                        photoURL: url
                    });
                });
              }); 
        })
        
    });

    function init() {
        firebase.auth().onAuthStateChanged(function (user) {
            if(!user){
                alert("Please Log in");
                window.location = "signin.html";
            }
        });
    }

    window.onload = function () {
        init();
    }