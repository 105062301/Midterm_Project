function init() {
    var user_email = '';
    var user_img = '';
    var user_id = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            user_img = user.photoURL;
            user_id = user.uid;
            menu.innerHTML = "<span>"+user.email+"</span><span class='dropdown-item' id='mypage-btn'>Mypage</span>"+"<span class='dropdown-item' id='logout-btn'>Logout</span>";
              
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out')
                        window.location = "signin.html";
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            var mypage_button = document.getElementById('mypage-btn');
            mypage_button.addEventListener('click', function () {
                window.location = "mydata.html";
            });


            
            var str_after_content = "</p></div></div>\n";

            var email;
            var data;
            var Ref = firebase.database().ref('com_list/'+keydata);
            var total_post = [];
            Ref.once('value')
            .then(function (snapshot) {
                    email = snapshot.val().email;
                    data = snapshot.val().data;
                    var storageRef = firebase.storage().ref(snapshot.val().userid + "/Img.jpg");
                    var str_before_username2 = "<div class='my-3 p-3 rounded box-shadow' style= 'background-color:rgba(255, 255, 255, 1)'><h6 class='border-bottom border-gray pb-2 mb-0'>Post</h6><div class='media text-muted pt-3'><img src='' alt='' class='"+snapshot.val().userid+"' style='height:50px; width:50px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                    storageRef.getDownloadURL().then(function(url) {
                        user_img = url;
                        $("."+snapshot.val().userid).each(function(){
                            this.src = user_img;
                        });
                    }).catch(function(error){
                        if(user.photoURL == null){
                            user_img ='img/imouto.jpg';
                        }
                        else{
                            user_img = user.photoURL;
                        }
                       $("."+snapshot.val().userid).each(function(){
                           this.src = user_img;
                       });
                    });
                    total_post[0] = str_before_username2 + email +  "</strong>" + data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
            })
            .catch(e => console.log(e.message));
            
            var postsRef = firebase.database().ref('com_list/'+keydata+'/post');
            var main_post;
            var first_count = 0;
            var second_count = 0;
    
             postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        var storageRef = firebase.storage().ref(childData.userid + "/Img.jpg");
                        var str_before_username = "<div class='my-3 p-3 rounded box-shadow' style= 'background-color:rgba(255, 255, 255, 0.6)'><h6 class='border-bottom border-gray pb-2 mb-0'>Reply</h6><div class='media text-muted pt-3'><img src='' alt='' class='"+childData.userid+"' style='height:50px; width:50px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                        if(childData.email!='undefined'){
                            storageRef.getDownloadURL().then(function(url) {
                                user_img = url;
                                $("."+childSnapshot.val().userid).each(function(){
                                    this.src = user_img;
                                });
                            }).catch(function(error){
                                if(user.photoURL == null){
                                    user_img ='img/imouto.jpg';
                                }
                                else{
                                    user_img = user.photoURL;
                                }
                               $("."+childSnapshot.val().userid).each(function(){
                                   this.src = user_img;
                               });
                            });
                            total_post[total_post.length+1] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                            first_count += 1;
                        }

            });

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var storageRef = firebase.storage().ref(childData.userid + "/Img.jpg");
                    var str_before_username = "<div class='my-3 p-3 rounded box-shadow' style= 'background-color:rgba(255, 255, 255, 0.6)'><h6 class='border-bottom border-gray pb-2 mb-0'>Reply</h6><div class='media text-muted pt-3'><img src='' alt='' class='"+childData.userid+"' style='height:50px; width:50px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
                    if(childData.email!='undefined'){
                        storageRef.getDownloadURL().then(function(url) {
                            user_img = url;
                            $("."+childData.userid).each(function(){
                                this.src = user_img;
                            });
                        }).catch(function(error){
                            if(user.photoURL == null){
                                user_img ='img/imouto.jpg';
                            }
                            else{
                                user_img = user.photoURL;
                            }
                           $("."+childData.userid).each(function(){
                               this.src = user_img;
                           });
                        });
                    }
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');

                    if (Notification.permission !== "granted")
                    Notification.requestPermission();
                    else {
                        var notification = new Notification('Notification', {
                            icon: 'img/icon.png',
                            body: "A message have been send to Arcadia",
                        });
                        notification.onclick = function () {
                            window.open("https://software-studio-105062301.firebaseapp.com");      
                        };
                    }
                    init();
                }
            });
        })
        .catch(e => console.log(e.message));

            
        } else {
            window.location = "signin.html";
            /*menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";*/
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    var query = window.location.search;
    // Skip the leading ?, which should always be there, 
    // but be careful anyway
    if (query.substring(0, 1) == '?') {
      query = query.substring(1);
    }
    var keydata = query.split(',');

    

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list/'+keydata+'/post').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value,
                userid: user_id
                //url: 
            });
            post_txt.value = "";

            document.addEventListener('DOMContentLoaded', function () {
                if (!Notification) {
                    alert('Desktop notifications not available in your browser. Try Chromium.'); 
                    return;
                } 
                if (Notification.permission !== "granted")
                    Notification.requestPermission();
            });
            init();
        }
    });

    

        function postpage(){
            window.location = "postpage.html";
        }
        
}



window.onload = function () {
    init();
}