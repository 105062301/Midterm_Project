var flag = 0;

function openNav() {
    if(flag==0){
        document.getElementById("mySidenav").style.width = "200px";
        document.getElementById("bd").style.marginLeft = "200px";
        flag = 1;
    }
    else{
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("bd").style.marginLeft = "0%";
        flag = 0;
    }
    
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("bd").style.marginLeft = "0%";
    flag = 0;
}

function logout(){
    firebase.auth().signOut();
    window.location = "signin.html";
}


function make(){
    window.location = "makemydata.html";
}


function init() {
    var user_email = '';
    var user_imgurl = '';
    var keydata = '';
   firebase.auth().onAuthStateChanged(function (user) {
        
        if (user) {
            var query = window.location.search;
            if (query.substring(0, 1) == '?') {
            query = query.substring(1);
            }
            keydata = query.split(',');
            

            user_email = user.email;  
             
                var dataRef = firebase.database().ref('/'+keydata);
                    dataRef.once('value')
                    .then(function (snapshot) {
                        var Data = snapshot.val();
                        var email = Data.email;
                        var name = Data.username;
                        var sex = Data.sex;
                        var birth = Data.birth;
                        var hobby = Data.hobby;
                        var motto = Data.motto;
                        var menu = document.getElementById('data_list');
                        menu.innerHTML = "<a>Owner's Email: "+ email +"</a><br><a>Name: "+ name +"</a><br><a>Sex: "+ sex +"</a><br><a>Birth: "+ birth +"</a><br><a>Hobby: "+ hobby +"</a><br><a>Favorite Motto: "+ motto +"</a>";
                           
                    
                    })
                    .catch(e => console.log(e.message));

                    var menu = document.getElementById('Img');

                    var storageRef = firebase.storage().ref('/'+keydata + "/Img.jpg");
                    storageRef.getDownloadURL().then(function(url) {
                        user_img = url;
                        $(".img1").each(function(){
                            this.src = user_img;
                        });
                    }).catch(function(error){
                        if(user.photoURL == null){
                            user_img ='img/imouto.jpg';
                        }
                        else{
                            user_img = user.photoURL;
                        }
                       $(".img1").each(function(){
                           this.src = user_img;
                       });
                    });

                    menu.innerHTML = "<a style='margin:0px; padding:0px'><img class='img1' id='top-right-img' src='' alt='Upload Your Picture!' style='width:80%; height:80%; margin:0px; padding:0px'></a>";
               
        } 
        else{
            alert("Please Log in");
            window.location = "signin.html";
        }
    
   });
}

    window.onload = function () {
        init();
    }
    
